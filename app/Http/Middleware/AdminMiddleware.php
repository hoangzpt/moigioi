<?php

namespace App\Http\Middleware;

use App\Enums\UserRoleEnum;
use Auth;
use Closure;
use Illuminate\Http\Request;

class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $arr = [
            UserRoleEnum::SUPER_ADMIN,
            UserRoleEnum::ADMIN,
        ];
        if (!auth()->check() || !in_array(user()->role, $arr) ) {
            return redirect()->route('login');
        }
        
        return $next($request);
    }
}
