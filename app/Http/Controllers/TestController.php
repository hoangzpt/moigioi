<?php

namespace App\Http\Controllers;

use App\Enums\FileTypeEnum;
use App\Enums\PostCurrencySalaryEnum;
use App\Enums\StatusEnum;
use App\Models\Company;
use App\Models\File;
use App\Models\Language;
use App\Models\Post;
use App\Models\User;
use DB;
use Illuminate\Http\Request;
use View;

class TestController extends Controller
{
    private object $model;
    private string $table;
    private string $title;

    public function __construct()
    {
        $this->model = User::query();
        $this->table = (new User())->getTable();
        $this->title = ucwords($this->table);

        View::share('title', $this->title);
        View::share('table', $this->table);
    }

    public function test() {
        // return DB::getSchemaBuilder()->getColumnListing('companies');
        // $companyName = 'Da Cap';
        // $language = 'PHP, Java, Python';
        // $city = 'HN';
        // $link = 'abc';

        // $company = Company::firstOrCreate([
        //     'name' => $companyName,
        // ], [
        //     'city' => $city,
        //     'country' => 'Vietnam',
        // ]);

        // $post = Post::create([
        //     'job_title' => $language,
        //     'company_id' => $company->id,
        //     'city' => $city,
        //     'status' => StatusEnum::ADMIN_APPROVE,
        // ]);

        // $languages = explode(', ', $language);
        // foreach ($languages as $language) {
        //     Language::firstOrCreate([
        //         'name' => $language,
        //     ]);
        // }

        // File::create([
        //     'post_id' => $post->id,
        //     'link' => $link,
        //     'type' => FileTypeEnum::JD,
        // ]);
        $val = PostCurrencySalaryEnum::EUR;
        $key = PostCurrencySalaryEnum::getKey($val);
        $locale = PostCurrencySalaryEnum::getLocaleByVal($val);
        $format = new \NumberFormatter($locale, \NumberFormatter::CURRENCY);
        $string = $format->formatCurrency(1000, $key);
        echo $string;
    }
}
