<?php

namespace App\Http\Controllers;

use App\Http\Requests\Company\StoreRequest;
use App\Models\Company;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use Throwable;

class CompanyController extends Controller
{
    use ResponseTrait;
    private object $model;

    public function __construct()
    {
        $this->model = Company::query();
    }

    public function index(Request $request): JsonResponse
    {
        $data = $this->model->where('name', 'like', '%' . $request->get('q') . '%')->get();

        return $this->successResponse($data);
    }

    public function show(Request $request): JsonResponse
    {
        $data = $this->model->paginate(5);
        $arr['data'] = $data->getCollection();
        $arr['pagination'] = $data->linkCollection();

        return $this->successResponse($arr);
    }

    public function check($companyName): JsonResponse
    {
        $check = $this->model->where('name', $companyName)->exists();
        return $this->successResponse($check);
    }
    public function store(StoreRequest $request): JsonResponse
    {
        try {

            $arr = $request->validated();
            // $arr['logo'] = optional($request->file('logo'))->store('company_logo');

            $logo = $request->file('logo');
            $file_name = time() . '.' . $logo->getClientOriginalExtension();
            $path = public_path('storage/' . $file_name);
            Image::make($logo->getRealPath())->resize(100, 100)->save($path);
            $arr['logo'] = $file_name;

            Company::create($arr);

            return $this->successResponse();

        } catch (Throwable $e) {
            return $this->errorResponse($e->getMessage());
        }

    }
}
