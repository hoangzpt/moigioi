<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Doctrine\DBAL\Types\JsonType;
use Illuminate\Http\Request;

class PostController extends Controller
{
    use ResponseTrait;
    private object $model;

    public function __construct()
    {
        $this->model = Post::query();
    }

    public function index() {
        $posts = $this->model->paginate();
        foreach ($posts as $post) {
            $post->currency_salary = $post->currency_salary_code;
            $post->status = $post->status_name;
        }
    

        $arr['data'] = $posts->getCollection();
        $arr['pagination'] = $posts->linkCollection();

        return $this->successResponse($arr);
    }
}
