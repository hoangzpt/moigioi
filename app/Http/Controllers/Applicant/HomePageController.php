<?php

namespace App\Http\Controllers\Applicant;

use App\Enums\PostRemotableEnum;
use App\Enums\StatusEnum;
use App\Http\Controllers\Controller;
use App\Http\Requests\Home\IndexRequest;
use App\Http\Requests\Home\IndexResquest;
use App\Models\Config;
use App\Models\Post;
use Illuminate\Http\Request;

class HomePageController extends Controller
{
    public function index(IndexRequest $request)
    {
        //Get Array City
        $cities = Post::pluck('city');
        $arrCity = [];

        foreach ($cities as $city) {
            if (!in_array($city, $arrCity)) {
                array_push($arrCity, $city);
            }
        }

        //Get Post
        $query = Post::with([
                'language',
                'company:id,name,logo',
            ])
            ->where('status', StatusEnum::ADMIN_APPROVE)
            ->orderByDesc('is_pinned')
            ->orderByDesc('id');

        //Filter City
        $filterCities = $request->get('cities', []);
        if (!empty($filterCities)) {
            $query->whereIn('city', $filterCities);
        }

        //Filter Salary
        $configs = Config::where('is_public', false)->get();
        $arrConfig = [];
        foreach ($configs as $config) {
            $arrConfig[$config->key] = $config->value;
        }
        $minSalary = $request->get('min_salary', $arrConfig['filter_min_salary']);
        $maxSalary = $request->get('max_salary', $arrConfig['filter_max_salary']);
        if($request->has('min_salary')) {
            $query->where('min_salary', '>=', $minSalary);
        }
        if($request->has('max_salary')) {
            $query->where('max_salary', '<=', $maxSalary);
        }

        //Filter Partime
        $filterPartime = $request->get('partime');
        if(!empty($filterPartime)) {
            $query->where('is_partime', 1);
        }

        //Filter Remotable
        $filterRemotable = $request->get('remotable');
        if (!empty($filterRemotable)) {
            $query->where('remotable', $filterRemotable);
        }

        $posts = $query->paginate()->withQueryString();

        //Add more attribute
        foreach ($posts as $post) {
            $post->salary_name = $post->salary;
            $post->expired = $post->is_expired;
        }

        //Get Array Remotable
        $filterPostRemotable = PostRemotableEnum::getArrWithLowerKey();

        return view('applicant.index', [
            'posts' => $posts,
            'arrCity' => $arrCity,
            'filterCities' => $filterCities,
            'minSalary' => $minSalary,
            'maxSalary' => $maxSalary,
            'filterPostRemotable' => $filterPostRemotable,
            'filterPartime' => $filterPartime,
            'filterRemotable' => $filterRemotable,
        ]);
    }

    public function show($postId)
    {
        $post = Post::with('file')
                ->where('status', StatusEnum::ADMIN_APPROVE)
                ->findOrFail($postId);

        return view('applicant.show', [
            'post' => $post,
        ]);
    }
}
