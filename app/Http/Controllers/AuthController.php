<?php

namespace App\Http\Controllers;

use App\Enums\UserRoleEnum;
use App\Http\Requests\Auth\RegisteringRequest;
use Illuminate\Http\Request;
use App\Models\User;
use Hash;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;

use function PHPUnit\Framework\isNull;

class AuthController extends Controller
{
    public function login()
    {
        return view('auth.login');
    }

    public function register()
    {
        $roles = UserRoleEnum::getRoleForRegister();
        // dd($roles);
        return view('auth.register', [
            'roles' => $roles,
        ]);
    }

    public function callback($provider)
    {
        $data = Socialite::driver($provider)->user();

        $user = User::whereEmail($data->getEmail())->first();
        $checkExist = true;

        if (is_null($user)) {
            $user = new User();
            $user->email = $data->getEmail();
            $user->role = UserRoleEnum::APPLICANT;
            $checkExist = false;
        }

        $user->name = $data->getName();
        $user->avatar = $data->getAvatar();

        Auth::login($user, true);

        if ($checkExist) {
            $role = getRoleByKey($user->role);
            if ($role === 'super_admin' || $role === 'admin'){
                return redirect()->route("admin.welcome");
            }
            return redirect()->route("$role.welcome");
        }

        return redirect()->route('register');
    }

    public function registering(RegisteringRequest $request)
    {
        // dd($request->all());
        $password = Hash::make($request->get('password'));
        $role = (int) $request->get('role');

        if (auth()->check()) {
            User::whereId(user()->id)->update([
                'password' => $password,
                'role' => $role,
            ]);
        } else {
            User::create([
                'name' => $request->get('name'),
                'email' => $request->get('email'),
                'password' => $password,
                'role' => $role,
            ]);
        }

        $role = strtolower(UserRoleEnum::getKey(user()->role));

        return redirect()->route("$role.welcome");
    }

    public function logout()
    {
        Auth::logout();

        return redirect()->route('login');
    }
}
