<?php

namespace App\Http\Controllers\Admin;

use App\Enums\CompanyCountryEnum;
use App\Http\Controllers\Controller;
use App\Http\Requests\Company\SaveRequest;
use App\Models\Company;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use View;

class CompanyController extends Controller
{
    private string $title;
    private string $table;

    public function __construct()
    {
        $this->table = (new Company())->getTable();
        $this->title = ucwords($this->table);

        View::share('title', $this->title);
        View::share('table', $this->table);
    }

    public function index()
    {
        $companies = Company::paginate(5);

        return view('admin.companies.index', [
            'companies' => $companies,
        ]);
    }

    public function create()
    {
        $countries = CompanyCountryEnum::asArray();

        return view('admin.companies.create', [
            'countries' => $countries,
        ]);
    }

    public function save(Request $request)
    {
        $company = new Company();
        $company->name = $request->name;
        $company->country = $request->country;
        $company->city = $request->city;
        $company->district = $request->district;
        $company->address = $request->address;
        $company->zipcode = $request->zipcode;
        $company->phone = $request->phone;
        $company->email = $request->email;
        //save logo
        $logo = $request->file('logo');
        $file_name = time() . '.' . $logo->getClientOriginalExtension();
        $path = public_path('storage/' . $file_name);
        Image::make($logo->getRealPath())->resize(100, 100)->save($path);
        $company->logo = $file_name;

        $company->save();

        return redirect()->route('admin.companies.index');
    }
}
