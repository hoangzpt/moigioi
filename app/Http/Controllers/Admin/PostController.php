<?php

namespace App\Http\Controllers\Admin;

use App\Enums\CompanyCountryEnum;
use App\Enums\ObjectLanguageTypeEnum;
use App\Enums\PostCurrencySalaryEnum;
use App\Enums\PostRemotableEnum;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ResponseTrait;
use App\Http\Requests\Posts\StoreRequest;
use App\Imports\PostsImport;
use App\Models\Company;
use App\Models\ObjectLanguage;
use App\Models\Post;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use View;

class PostController extends Controller
{
    private object $model;
    private string $table;
    private string $title;
    use ResponseTrait;

    public function __construct()
    {
        $this->model = Post::query();
        $this->table = (new Post())->getTable();
        $this->title = ucwords($this->table);

        View::share('title', $this->title);
        View::share('table', $this->table);
    }

    public function index() {
        $data = $this->model->get();

        return view('admin.posts.index', [
            'data' => $data,
        ]);
    }

    public function create() {
        $currencies = PostCurrencySalaryEnum::asArray();
        $countries = CompanyCountryEnum::asArray();

        return view('admin.posts.create', [
            'currencies' => $currencies,
            'countries' => $countries,
        ]);
    }

    public function store(StoreRequest $request) {
        try {
            $arr = $request->only([
                'city',
                'currency_salary',
                'district',
                'end_date',
                'is_partime',
                'is_pinned',
                'job_title',
                'remotable',
                'max_salary',
                'min_salary',
                'number_applicants',
                'requirement',
                'slug',
                'start_date',
            ]);

            $companyName = $request->get('company');
            if (!empty($companyName)) {
                $arr['company_id'] = Company::firstOrCreate(['name' => $companyName])->id;
            }

            if ($request->has('remotable')) {
                $remotable = $request->get('remotable');
                if (!empty($remotable['remote']) && !empty($remotable['office'])) {
                    $arr['remotable'] = PostRemotableEnum::DYNAMIC;
                } elseif (!empty($remotable['remote'])) {
                    $arr['remotable'] = PostRemotableEnum::REMOTE_ONLY;
                } else {
                    $arr['remotable'] = PostRemotableEnum::OFFICE_ONLY;
                }
            }


            if ($request->has('is_partime')) {
                $arr['is_partime'] = 1;
            }

            $post = Post::create($arr);

            $languages = $request->get('languages');
            foreach($languages as $language) {
                ObjectLanguage::create([
                    'language_id' => $language,
                    'object_id' => $post->id,
                    'object_type' => 'App\Models\Post',
                ]);
            }

            return $this->successResponse();

        } catch (\Throwable $e) {
            return $this->errorResponse($e->getMessage());
        }
    }

    public function importCSV(Request $request) {
        Excel::import(new PostsImport, $request->file('csv'));
    }
}
