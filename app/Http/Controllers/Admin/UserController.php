<?php

namespace App\Http\Controllers\Admin;

use App\Enums\UserRoleEnum;
use App\Http\Controllers\Controller;
use App\Models\Company;
use App\Models\User;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;
use View;

class UserController extends Controller
{
    private object $model;
    private string $table;
    private string $title;

    public function __construct()
    {
        $this->model = User::query();
        $this->table = (new User())->getTable();
        $this->title = ucwords($this->table);

        View::share('title', $this->title);
        View::share('table', $this->table);
    }

    public function index(Request $request) {
        $selectedRole = $request->get('role');
        $selectedCity = $request->get('city');
        $selectedCompany = $request->get('company');

        $query = $this->model
            ->with('company:id,name');

        if (!empty($selectedRole) && $selectedRole != 'All') {
            $query->where('role', $selectedRole);
        }
        if (!empty($selectedCity) && $selectedCity != 'All') {
            $query->where('city', $selectedCity);
        }
        if (!empty($selectedCompany) && $selectedCompany != 'All') {
            $query->where('company_id', $selectedCompany);
        }

        $data = $query->paginate();

        $cities = User::distinct()->pluck('city');
        $companies = Company::select('id', 'name')->get();
        $roles = UserRoleEnum::asArray();

        return view("admin.$this->table.index", [
            'data' => $data,
            'roles' => $roles,
            'cities' => $cities,
            'companies' => $companies,
            'selectedRole' => $selectedRole,
            'selectedCity' => $selectedCity,
            'selectedCompany' => $selectedCompany,
        ]);
    }

    public function destroy($userId) {
        User::destroy($userId);
        return redirect()->back();
    }
}
