<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Post extends Component
{
    public object $posts;
    public string $languages;
    public object $company;
    
    public function __construct($post)
    {
        $this->posts = $post;
        $this->languages = $post->language->pluck('name')->implode(', ');
        $this->company = $post->company;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.post');
    }
}
