<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static OptionOne()
 * @method static static OptionTwo()
 * @method static static OptionThree()
 */
final class PostRemotableEnum extends Enum
{
    public const REMOTE_ONLY = 1;
    public const OFFICE_ONLY = 2;
    public const DYNAMIC = 3;

    public static function getArrWithLowerKey(): array
    {
        $data = self::asArray();
        $arr = ['all' => 0];

        foreach($data as $key => $value) {
            $index = strtolower(str_replace('_', ' ', $key));
            $arr[$index] = $value;
        }
        
        return $arr;
    }

    // fix IndexRequest validate gia tri 0 trong remotable
    public static function getArrRemotableWithAll(): array
    {
        $data = self::getValues();
        $arr = ['4' => 0];

        foreach($data as $key => $value) {
            $arr[$key] = $value;
        }
        
        return $arr;
    }
}
