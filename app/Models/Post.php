<?php

namespace App\Models;

use App\Enums\PostCurrencySalaryEnum;
use App\Enums\PostRemotableEnum;
use App\Enums\StatusEnum;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Str;

use function PHPUnit\Framework\isNull;

class Post extends Model
{
    use HasFactory;

    protected $fillable = [
        'city',
        'company_id',
        'currency_salary',
        'district',
        'end_date',
        'is_partime',
        'is_pinned',
        'job_title',
        'max_salary',
        'min_salary',
        'number_applicants',
        'remotable',
        'requirement',
        'slug',
        'start_date',
        'status',
        'user_id'
    ];

    protected static function booted() {
        static::creating(static function ($object) {
            // $object->user_id = auth()->id();
            $object->user_id = user()->id;
            $object->status = StatusEnum::getByRole();
        });
    }

    public function language()
    {
        return $this->morphToMany(Language::class, 'object', ObjectLanguage::class, 'object_id', 'language_id');
    }

    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    public function file()
    {
        return $this->hasOne(File::class);
    }

    public function getCurrencySalaryCodeAttribute(): string
    {
        return PostCurrencySalaryEnum::getKey($this->currency_salary);
    }

    public function getRemotableNameAttribute(): string
    {
        $key = PostRemotableEnum::getKey($this->remotable);
        $arr = explode('_', $key);
        $str = '';
        foreach ($arr as $each) {
            $str .= Str::title($each) . ' ';
        }

        return $str;
    }

    public function getStatusNameAttribute(): string
    {
        return StatusEnum::getKey($this->status);
    }

    public function getSalaryAttribute(): string
    {
        $val = $this->currency_salary;
        $key = PostCurrencySalaryEnum::getKey($val);
        $locale = PostCurrencySalaryEnum::getLocaleByVal($val);
        $format = new \NumberFormatter($locale, \NumberFormatter::CURRENCY);

        if(!is_null($this->min_salary)) {
            $minSalary = $format->formatCurrency($this->min_salary, $key);
        }

        if(!is_null($this->max_salary)) {
            $max_salary = $format->formatCurrency($this->max_salary, $key);
        }

        if (!empty($minSalary) && !empty($max_salary)) {
            return $minSalary . ' - ' . $max_salary;
        }

        if (!empty($minSalary)) {
            return 'From' . ' ' . $minSalary;
        }

        if (!empty($max_salary)) {
            return 'To' . ' ' . $max_salary;
        }

        return '';
    }

    public function getIsExpiredAttribute(): bool
    {
        $now = date('Y-m-d');
        if ($now >= $this->end_date && !is_null($this->end_date)) {
            return true;
        }

        return false;
    }
}
