<?php

namespace App\Models;

use App\Enums\UserRoleEnum;
use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Authenticatable as AuthAuthenticatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class User extends Model implements AuthAuthenticatable
{
    use HasFactory;
    use Authenticatable;

    protected $fillable = [
        'email',
        'name',
        'avatar',
    ];

    public function company() {
        return $this->belongsTo(Company::class);
    }

    public function getRoleNameAttribute() {
        return UserRoleEnum::getKeys($this->role)[0];
    }

    public function getGenderNameAttribute() {
        return ($this->gender == 0) ? 'Male' : 'Female';
    }
}
