@extends('layout_page.master')

@section('content')
    <div class="row">
        <div class="col-md-4 col-sm-6">

            <div class="tab-content">
                <div class="card card-pricing">
                    <div class="card-content">
                        <ul>
                            <li>{{ $post->remotable_name }}</li>
                            @if ($post->is_partime)
                                <li>
                                    <i class="material-icons text-success">
                                        check
                                    </i>
                                    Part-time
                                </li>
                            @else
                                <li>
                                    <i class="material-icons text-danger">
                                        close
                                    </i>
                                    Part-time
                                </li>
                            @endif
                            @isset($post->number_applicants)
                                <li><i class="material-icons text-success">check</i> 
                                    Number applicants: {{ $post->number_applicants }}
                                </li>
                            @endisset
                            @if(!empty($post->start_date) && !empty($post->end_date))
                                <li>
                                    Date: {{ $post->start_date }} - {{ $post->end_date }}
                                </li>
                            @endif
                        </ul>
                        
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-sm-6">
            <h2 class="title"> 
                {{ $post->job_title }}
            </h2>
            <h3 class="main-price">
                {{ $post->salary }}
            </h3>
            <h4>
                <a href="@">
                    {{ $post->company->name }}
                </a>
            </h4>
            <h4>
                {{ $post->district }} - {{ $post->city }}
            </h4>

            @if (!empty($post->requirement))
            <div id="acordeon">
                <div class="panel-group" id="accordion">
                    <div class="panel panel-border panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne"
                                aria-expanded="true" aria-controls="collapseOne">
                                <h4 class="panel-title">
                                    Requirement
                                    <i class="material-icons">keyboard_arrow_down</i>
                                </h4>
                            </a>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse in">
                            <div class="panel-body">
                                <p>{{ $post->requirement }}</p>
                            </div>
                        </div>
                    </div>

                </div>
            </div><!--  end acordeon -->
            @endif

            @isset($post->file)
                <div class="row text-right">
                    <a class="btn btn-rose btn-round" href="{{ $post->file->link }}" target="_blank">
                        Open file &nbsp;
                        <i class="fa fa-file"></i>
                    </a>
                </div>
            @endisset
        </div>
    </div>
@endsection
