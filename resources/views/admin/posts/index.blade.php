@extends('layouts.master')

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <a href="{{ route('admin.posts.create') }}" class="btn btn-primary">
                        Create
                    </a>
                    <label class="btn btn-success mb-0" for="csv">Import CSV</label>
                    <input type="file" class="form-control-file d-none" name="csv" id="csv"
                        accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel">
                    <nav class="float-right">
                        <ul id="pagination" class="pagination pagination-rounded mb-0">
                        </ul>
                    </nav>
                </div>
                <div class="card-body">
                    <table id="table-data" class="table table-striped">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Job Title</th>
                                <th>Location</th>
                                <th>Remotable</th>
                                <th>Is Part-time</th>
                                <th>Range Salary</th>
                                <th>Date Range</th>
                                <th>Status</th>
                                <th>Is Pinned</th>
                                <th>Created At</th>
                            </tr>
                        </thead>
                        <tbody>
                            
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')
    <script>
        $(document).ready(function() {
            $.ajax({
                url: '{{ route('api.posts') }}',
                data: {page: {{ request()->get('page') ?? 1 }} },
                dataType: "json",
                success: function(response) {
                    response.data.data.forEach(function (each) {
                        let location = (each.district && each.city) ? each.district + ' - ' + each.city : '';
                        let remotable = each.remotable ? 'x' : '';
                        let is_partime = each.is_partime ? 'x' : '';
                        let range_salary = (each.min_salary && each.max_salary) ? each.min_salary + ' - ' + each.max_salary : '';
                        let range_date = (each.start_date && each.end_date) ? each.start_date + ' - ' + each.end_date : '';
                        let is_pinned = each.is_pinned ? 'x' : '';
                        let created_at = new Date(each.created_at);
                        $('#table-data').append($('<tr>')
                            .append($('<td>').append(each.id))    
                            .append($('<td>').append(each.job_title))    
                            .append($('<td>').append(location))    
                            .append($('<td>').append(remotable))
                            .append($('<td>').append(is_partime))    
                            .append($('<td>').append(range_salary))    
                            .append($('<td>').append(range_date))    
                            .append($('<td>').append(each.status))    
                            .append($('<td>').append(is_pinned))    
                            .append($('<td>').append(created_at))    
                        )
                    });

                    response.data.pagination.forEach(function (each){
                        $('#pagination').append($('<li>').attr('class', `page-item ${each.active ? 'active' : ''}`)
                            .append(`<a class="page-link" href="${each.url}">${each.label}</a>`)
                        );
                    });

                    $(document).on('click', '#pagination > li > a', function (event) {
                        event.preventDefault();
                        let page = $(this).text();
                        let urlParams = new URLSearchParams(window.location.search);
                        urlParams.set('page', page);
                        window.location.search = urlParams;
                    });
                }
            });
            
            

            // $('#csv').change(function(e) {
            //     var formData = new FormData();
            //     formData.append("file", $(this)[0].files[0]);

            //     $.ajax({
            //         url: '{{ route('admin.posts.importCSV') }}',
            //         type: 'POST',
            //         dataType: 'json',
            //         data: formData,
            //         async: false,
            //         cache: false,
            //         contentType: false,
            //         enctype: 'multipart/form-data',
            //         processData: false,
            //         success: function(response) {
            //             $.toast({
            //                 heading: 'Import Success',
            //                 text: 'Your data has been imported',
            //                 position: 'bottom-right',
            //                 showHideTransition: 'slide',
            //                 icon: 'success'
            //             })
            //         }
            //     });
            // });
        });
    </script>
@endpush
