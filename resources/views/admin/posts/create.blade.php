@extends('layouts.master')

@push('css')
    <link rel="stylesheet" href="{{ asset('CSS/summernote-bs4.css') }}">
@endpush

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div id="div-error" class="alert alert-danger d-none"></div>
                <form action="{{ route('admin.posts.store') }}" method="POST" id="form-create-post">
                    @csrf
                    <div class="card-body">
                        <div class="form-group">
                            <label for="select-company">Company</label>
                            <select class="form-control" name="company" id="select-company">
                                <option></option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="language">Language</label>
                            <select aria-label="language" class="form-control" multiple name="languages[]"
                                id="select-language">
                                <option></option>
                            </select>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-6">
                                <label for="select-city">City</label>
                                <select class="form-control citySelect" name="city" id="select-city">
                                    <option></option>
                                </select>
                            </div>
                            <div class="form-group col-6">
                                <label for="select-district">District</label>
                                <select class="form-control districtSelect" name="district" id="select-district">
                                    <option></option>
                                </select>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-12">
                                <label for="min_salary">Min-Max Salary</label>
                                <div class="d-flex justify-content-around" style="gap: 15px">
                                    <input type="number" name="min_salary" id="min_salary" class="form-control">
                                    <input type="number" name="max_salary" id="max_salary" class="form-control">
                                    <select class="form-control" name="currency_salary" id="">
                                        @foreach ($currencies as $currency => $value)
                                            <option value="{{ $value }}">
                                                {{ $currency }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-9">
                                <label for="text-requirement">Requirement</label>
                                <textarea class="form-control" name="requirement" id="text-requirement"></textarea>
                            </div>
                            <div class="form-group col-1 d-flex flex-column">
                                <label for="">Remotable</label>
                                <input type="checkbox" id="remote" name="remotable[remote]" checked data-switch="success"/>
                                <label for="remote" data-on-label="Yes" data-off-label="No"></label>
                            </div>
                            <div class="form-group col-1 d-flex flex-column">
                                <label for="">Office</label>
                                <input type="checkbox" id="office" name="remotable[office]" checked data-switch="success"/>
                                <label for="office" data-on-label="Yes" data-off-label="No"></label>
                            </div>
                            <div class="form-group col-1 d-flex flex-column">
                                <label for="">Is Partime</label>
                                <input type="checkbox" id="is_partime" checked data-switch="success"/>
                                <label for="is_partime" data-on-label="Yes" data-off-label="No"></label>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-12">
                                <label>Start-End Date</label>
                                <div class="d-flex justify-content-around" style="gap: 15px">
                                    <input type="date" name="start_date" class="form-control">
                                    <input type="date" name="end_date" class="form-control">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="">Number Applicant</label>
                            <input type="number" name="number_applicants" id="" class="form-control">
                        </div>

                        <div class="form-row">
                            <div class="form-group col-6">
                                <label for="title">Title</label>
                                <input type="text" name="job_title" class="form-control" onkeyup="generateSlug();"
                                    id="title">
                            </div>
                            <div class="form-group col-6">
                                <label for="slug">Slug</label>
                                <input type="text" name="slug" class="form-control" id="slug">
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary">Create</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Modal Create Company  -->
    <div id="modal-company" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="standard-modalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="standard-modalLabel">Create Company</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <form enctype="multipart/form-data" action="{{ route('admin.companies.store') }}" method="POST" id="form-create-company">
                        @csrf
                        <div class="form-group">
                            <label for="company">Company</label>
                            <input type="text" readonly name="name" id="company" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="address">Address</label>
                            <input type="text" name="address" id="address" class="form-control">
                        </div>
                        <div class="form-row">
                            <div class="form-group col-4">
                                <label for="city">City</label>
                                <select class="form-control" name="city" id="city">
                                </select>
                            </div>
                            <div class="form-group col-4">
                                <label for="district">District</label>
                                <select class="form-control" name="district" id="district">
                                </select>
                            </div>
                            <div class="form-group col-4">
                                <label for="country">Country</label>
                                <select class="form-control" name="country" id="country">
                                    @foreach ($countries as $val => $name)
                                        <option value="{{ $val }}">
                                            {{ $name }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-6">
                                <label for="zipcode">Zip-code</label>
                                <input type="number" class="form-control" name="zipcode">
                            </div>
                            <div class="form-group col-6">
                                <label for="phone">Phone</label>
                                <input type="number" class="form-control" name="phone">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="email" name="email" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Logo</label>
                            <input type="file" name="logo"
                                    oninput="pic.src=window.URL.createObjectURL(this.files[0])">
                            <img id="pic" height="100"/>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light">Close</button>
                    <button type="button" onclick="submitForm('company');" class="btn btn-primary">Create</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

@endsection

@push('js')
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.5/jquery.validate.js"></script>
    <script src="{{ asset('JS/summernote-bs4.min.js') }}"></script>
    <script>
        function generateTitle() {
            let languages = [];
            const selectedLanguages = $("#select-language :selected").map(function(i, v) {
                languages.push($(v).text());
            });
            languages = languages.join(', ');
            const city = $("#select-city").val();
            const company = $("#select-company option:selected").text();

            let string = `(${city}) - ${languages}`;
            if (company) {
                string += ' - ' + company;
            }

            $("#title").val(string);
        }

        function generateSlug() {
            var title;

            //Lấy text từ thẻ input title
            title = document.getElementById("title").value;
            title = title.toLowerCase();
            //Đổi ký tự có dấu thành không dấu
            title = title.replace(/á|à|ả|ạ|ã|ă|ắ|ằ|ẳ|ẵ|ặ|â|ấ|ầ|ẩ|ẫ|ậ/gi, 'a');
            title = title.replace(/é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ/gi, 'e');
            title = title.replace(/i|í|ì|ỉ|ĩ|ị/gi, 'i');
            title = title.replace(/ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ/gi, 'o');
            title = title.replace(/ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự/gi, 'u');
            title = title.replace(/ý|ỳ|ỷ|ỹ|ỵ/gi, 'y');
            title = title.replace(/đ/gi, 'd');
            //Xóa các ký tự đặt biệt
            title = title.replace(/\`|\~|\!|\@|\#|\||\$|\%|\^|\&|\*|\(|\)|\+|\=|\,|\.|\/|\?|\>|\<|\'|\"|\:|\;|_/gi, '');
            //Đổi khoảng trắng thành ký tự gạch ngang
            title = title.replace(/ /gi, "-");
            //Đổi nhiều ký tự gạch ngang liên tiếp thành 1 ký tự gạch ngang
            //Phòng trường hợp người nhập vào quá nhiều ký tự trắng
            title = title.replace(/\-\-\-\-\-/gi, '-');
            title = title.replace(/\-\-\-\-/gi, '-');
            title = title.replace(/\-\-\-/gi, '-');
            title = title.replace(/\-\-/gi, '-');
            //Xóa các ký tự gạch ngang ở đầu và cuối
            title = '@' + title + '@';
            title = title.replace(/\@\-|\-\@|\@/gi, '');
            //In slug ra textbox có id “slug”
            document.getElementById('slug').value = title;
        }

        async function loadDistrict() {
            $('#select-district').empty();
            const path = $("#select-city option:selected").data('path');
            const response = await fetch('{{ asset('locations/') }}' + path);
            const district = await response.json();
            $.each(district.district, function(index, each) {
                if (each.pre === 'Quận' || each.pre === 'Huyện' || each.pre === 'Thành phố' || each.pre ===
                    'Thị xã' || each.pre === 'Thị trấn') {
                    $('#select-district').append(`
                            <option>
                                ${each.name}
                            </option>
                        `);
                }

            })
        }

        async function loadDistrictModal() {
            $('#district').empty();
            const path = $("#city option:selected").data('path');
            const response = await fetch('{{ asset('locations/') }}' + path);
            const district = await response.json();
            $.each(district.district, function(index, each) {
                if (each.pre === 'Quận' || each.pre === 'Huyện' || each.pre === 'Thành phố' || each.pre ===
                    'Thị xã' || each.pre === 'Thị trấn') {
                    $('#district').append(`
                            <option>
                                ${each.name}
                            </option>
                        `);
                }

            })
        }

        function showError(errors) {
            let string = '<ul>';
            if (Array.isArray(errors)) {
                errors.forEach(function(each) {
                    each.forEach(function(error) {
                        string += `<li>${error}</li>`;
                    });
                });
            } else {
                string += `<li>${errors}</li>`;
            }
            string += '</ul>';
            $("#div-error").html(string).removeClass('d-none').show();
            $.toast({
                heading: 'Error',
                text: string,
                showHideTransition: 'slide',
                position: 'bottom-right',
                icon: 'error',
            });
        }

        async function checkCompany() {
            const response = await fetch('{{ route('api.companies.check') }}/' + $("#select-company").val());
            $.ajax({
                type: "get",
                url: '{{ route('api.companies.check') }}/' + $("#select-company").val(),
                dataType: "json",
                success: function(response) {
                    if (response.data) {
                        submitForm('post');
                    } else {
                        $("#modal-company").modal("show");
                        $("#company").val($("#select-company").val());
                    }
                }
            });
        }

        async function submitForm(type) {
            const obj = $("#form-create-"+type)
            const formData = new FormData(obj[0]);
            console.log(formData);
            $.ajax({
                url: obj.attr('action'),
                type: 'POST',
                dataType: 'json',
                data: formData,
                processData: false,
                contentType: false,
                async: false,
                cache: false,
                enctype: 'multipart/form-data',
                success: function(response) {
                    if (response.success) {
                        $("#div-error").hide();
                        $("#modal-company").modal('hide');
                        $.toast({
                            heading: 'Success',
                            text: 'Insert' + ' ' + type + ' ' + 'successfully',
                            showHideTransition: 'slide',
                            position: 'bottom-right',
                            icon: 'success',
                        });
                    } else {
                        showError(response.message);
                    }
                },
                error: function(response) {
                    let errors;
                    if (response.responseJSON.errors) {
                        errors = Object.values(response.responseJSON.errors);
                        showError(errors);
                    } else {
                        errors = response.responseJSON.message;
                        showError(errors);
                    }
                }
            })

        }

        $(document).ready(async function() {
            $("#text-requirement").summernote();

            //Company
            $("#select-company").select2({
                tags: true,
                ajax: {
                    url: '{{ route('api.companies') }}',
                    data: function(params) {
                        var queryParameters = {
                            q: params.term
                        }

                        return queryParameters;
                    },
                    processResults: function(data) {
                        return {
                            results: $.map(data.data, function(item) {
                                return {
                                    text: item.name,
                                    id: item.name,
                                }
                            })
                        };
                    },
                }
            });


            //Language
            $("#select-language").select2({
                ajax: {
                    url: '{{ route('api.languages') }}',
                    data: function(params) {
                        var queryParameters = {
                            q: params.term
                        }

                        return queryParameters;
                    },
                    processResults: function(data) {
                        return {
                            results: $.map(data.data, function(item) {
                                return {
                                    text: item.name,
                                    id: item.id,
                                }
                            })
                        };
                    },
                }
            });


            //City
            $("#select-city").select2();
            $("#city").select2();
            const response = await fetch('{{ asset('locations/index.json') }}');
            const cities = await response.json();
            $.each(cities, function(index, each) {
                $('#select-city').append(`
                        <option value='${index}' data-path='${each.file_path}'>
                            ${index}
                        </option>
                    `);
                $('#city').append(`
                        <option value='${index}' data-path='${each.file_path}'>
                            ${index}
                        </option>
                    `);
            })


            //District
            $('#select-city, #city').change(function() {
                loadDistrict();
                loadDistrictModal();
            })
            $("#select-district").select2();
            $("#district").select2();
            loadDistrict();
            loadDistrictModal();

            //title
            $(document).on('change', '#select-city, #select-company, #selected-language', function() {
                generateTitle();
            });

            //Validation Form Create
            $("#form-create-post").validate({
                rules: {
                    company: "required",
                },
                submitHandler: function() {
                    checkCompany();
                },
            })
        });


    </script>
@endpush
