@extends('layouts.master')

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <a href="{{ route('admin.companies.create') }}" class="btn btn-primary">
                        Create
                    </a>
                    <nav class="float-right">
                        <ul id="pagination" class="pagination pagination-rounded mb-0">
                        </ul>
                    </nav>
                </div>
                <div class="card-body">
                    <table id="table-data" class="table table-striped">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Location</th>
                                <th>Phone</th>
                                <th>Email</th>
                                <th>Logo</th>
{{--                                <th>Actions</th>--}}
                            </tr>
                        </thead>
                        <tbody>
{{--                            @foreach($companies as $company)--}}
{{--                                <tr>--}}
{{--                                    <td>{{ $company->id }}</td>--}}
{{--                                    <td>{{ $company->name }}</td>--}}
{{--                                    <td>{{ $company->district }} - {{ $company->city }}</td>--}}
{{--                                    <td>{{ $company->phone }}</td>--}}
{{--                                    <td>{{ $company->email }}</td>--}}
{{--                                    <td>--}}
{{--                                        <img src="{{ asset('storage/' . $company->logo) }}" width="50px">--}}
{{--                                    </td>--}}
{{--                                    <td>--}}
{{--                                        <a href="" class="btn btn-warning">Edit</a>--}}
{{--                                        <form action="" method="POST" class="d-inline">--}}
{{--                                            @csrf--}}
{{--                                            @method('DELETE')--}}
{{--                                            <button class="btn btn-danger" onclick="return confirm('Are you sure?')">Delete</button>--}}
{{--                                        </form>--}}
{{--                                    </td>--}}
{{--                                </tr>--}}
{{--                            @endforeach--}}
                        </tbody>
                    </table>
                    <nav>
                        <ul class="pagination pagination-rounded mb-0">
                            {{ $companies->links() }}
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')
    <script>
        $(document).ready(function() {
            $.ajax({
                url: '{{ route('api.companies.show') }}',
                data: {page: {{ request()->get('page') ?? 1 }} },
                dataType: "json",
                success: function(response) {
                    response.data.data.forEach(function (each) {
                        let location = (each.district && each.city) ? each.district + ' - ' + each.city : '';
                        let name = each.name;
                        let phone = each.phone ? each.phone : '';
                        let email = each.email ? each.email : '';
                        // let logo = each.logo ? `<img src=" 'storage/' + '' + ${each.logo} ">` : '';
                        let logo_src = "http://moigioi.test/storage/".concat("", each.logo);
                        let logo = each.logo ? `<img src="${logo_src}" width="50px">` : '';
                        console.log(logo_src);
                        $('#table-data').append($('<tr>')
                            .append($('<td>').append(each.id))
                            .append($('<td>').append(name))
                            .append($('<td>').append(location))
                            .append($('<td>').append(phone))
                            .append($('<td>').append(email))
                            .append($('<td>').append(logo))
                        )
                    });

                    response.data.pagination.forEach(function (each){
                        $('#pagination').append($('<li>').attr('class', `page-item ${each.active ? 'active' : ''}`)
                            .append(`<a class="page-link" href="${each.url}">${each.label}</a>`)
                        );
                    });

                    $(document).on('click', '#pagination > li > a', function (event) {
                        event.preventDefault();
                        let page = $(this).text();
                        let urlParams = new URLSearchParams(window.location.search);
                        urlParams.set('page', page);
                        window.location.search = urlParams;
                    });
                }
            });
        });
    </script>
@endpush
