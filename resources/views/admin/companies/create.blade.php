@extends('layouts.master')

@push('css')
    <link rel="stylesheet" href="{{ asset('CSS/summernote-bs4.css') }}">
@endpush

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div id="div-error" class="alert alert-danger d-none"></div>
                <form enctype="multipart/form-data" action="{{ route('admin.companies.store') }}" method="POST" id="form-create-company">
                    @csrf
                    <div class="card-body">
                        <div class="form-group">
                            <label for="company_name">Name</label>
                            <input required class="form-control" name="name" id="company_name"/>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-3">
                                <label for="company_address">Address</label>
                                <input required class="form-control" name="address" id="company_address"/>
                            </div>
                            <div class="form-group col-3">
                                <label for="select-city">City</label>
                                <select class="form-control citySelect" name="city" id="select-city">
                                    <option></option>
                                </select>
                            </div>
                            <div class="form-group col-3">
                                <label for="select-district">District</label>
                                <select class="form-control districtSelect" name="district" id="select-district">
                                    <option></option>
                                </select>
                            </div>
                            <div class="form-group col-3">
                                <label for="country">Country</label>
                                <select class="form-control" name="country" id="country">
                                    @foreach ($countries as $val => $name)
                                        <option value="{{ $val }}">
                                            {{ $name }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="">Zip Code</label>
                            <input type="number" name="zipcode" id="" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="">Email</label>
                            <input type="email" name="email" id="" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Logo</label>
                            <input type="file" name="logo"
                                   oninput="pic.src=window.URL.createObjectURL(this.files[0])">
                            <img id="pic" height="100"/>
                        </div>


                        <button type="button" onclick="submitForm()" class="btn btn-primary">Create</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection

@push('js')
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.5/jquery.validate.js"></script>
    <script>
        async function loadDistrict() {
            $('#select-district').empty();
            const path = $("#select-city option:selected").data('path');
            const response = await fetch('{{ asset('locations/') }}' + path);
            const district = await response.json();
            $.each(district.district, function(index, each) {
                if (each.pre === 'Quận' || each.pre === 'Huyện' || each.pre === 'Thành phố' || each.pre ===
                    'Thị xã' || each.pre === 'Thị trấn') {
                    $('#select-district').append(`
                            <option>
                                ${each.name}
                            </option>
                        `);
                }

            })
        }

        function showError(errors) {
            let string = '<ul>';
            if (Array.isArray(errors)) {
                errors.forEach(function(each) {
                    each.forEach(function(error) {
                        string += `<li>${error}</li>`;
                    });
                });
            } else {
                string += `<li>${errors}</li>`;
            }
            string += '</ul>';
            $("#div-error").html(string).removeClass('d-none').show();
            $.toast({
                heading: 'Error',
                text: string,
                showHideTransition: 'slide',
                position: 'bottom-right',
                icon: 'error',
            });
        }

        async function submitForm() {
            const obj = $("#form-create-company")
            const formData = new FormData(obj[0]);
            console.log(formData);
            $.ajax({
                url: obj.attr('action'),
                type: 'POST',
                dataType: 'json',
                data: formData,
                processData: false,
                contentType: false,
                async: false,
                cache: false,
                enctype: 'multipart/form-data',
                success: function(response) {
                    if (response.success) {
                        $("#div-error").hide();
                        $.toast({
                            heading: 'Success',
                            text: 'Insert' + ' ' + 'company' + ' ' + 'successfully',
                            showHideTransition: 'slide',
                            position: 'bottom-right',
                            icon: 'success',
                        });
                        window.location.href = '{{ route('admin.companies.index') }}';
                    } else {
                        showError(response.message);
                    }
                },
                error: function(response) {
                    let errors;
                    if (response.responseJSON.errors) {
                        errors = Object.values(response.responseJSON.errors);
                        showError(errors);
                    } else {
                        errors = response.responseJSON.message;
                        showError(errors);
                    }
                }
            })

        }

        $(document).ready(async function() {
            //City
            $("#select-city").select2();
            $("#city").select2();
            const response = await fetch('{{ asset('locations/index.json') }}');
            const cities = await response.json();
            $.each(cities, function(index, each) {
                $('#select-city').append(`
                        <option value='${index}' data-path='${each.file_path}'>
                            ${index}
                        </option>
                    `);
                $('#city').append(`
                        <option value='${index}' data-path='${each.file_path}'>
                            ${index}
                        </option>
                    `);
            })


            //District
            $('#select-city, #city').change(function() {
                loadDistrict();
            })
            $("#select-district").select2();
            $("#district").select2();
            loadDistrict();

        });


    </script>
@endpush
