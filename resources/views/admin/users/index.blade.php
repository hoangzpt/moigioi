@extends('layouts.master')

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <form class="form-horizontal" id="form-filter" action="">
                        <div style="gap: 20px;" class="form-group d-flex flex-row">
                            <div>
                                <label for="role">Role</label>
                                <select name="role" id="role" class="custom-select custom-select-sm mb-3 select-filter">
                                    <option selected>All</option>
                                    @foreach ($roles as $role => $value)
                                        <option value="{{ $value }}" 
                                        @if ((string)$value == $selectedRole) selected @endif
                                        >
                                            {{ $role }} 
                                        </option>
                                    @endforeach
                                </select> 
                            </div>
                            <div>
                                <label for="city">City</label>
                                <select name="city" id="city" class="custom-select custom-select-sm mb-3 select-filter">
                                    <option selected>All</option>
                                    @foreach ($cities as $city)
                                        <option value="{{ $city }}"
                                        @if ($city == $selectedCity) selected @endif
                                        >
                                            {{ $city }} 
                                        </option>
                                    @endforeach
                                </select> 
                            </div>
                            <div>
                                <label for="company">City</label>
                                <select name="company" id="company" class="custom-select custom-select-sm mb-3 select-filter">
                                    <option selected>All</option>
                                    @foreach ($companies as $company)
                                        <option value="{{ $company->id }}"
                                        @if ($company->id == $selectedCompany) selected @endif
                                        >
                                            {{ $company->name }} 
                                        </option>
                                    @endforeach
                                </select> 
                            </div>
                            
                        </div>
                    </form>
                </div>
                <div class="card-body">
                    <table class="table table-hover table-centered mb-0">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Avatar</th>
                                <th>Info</th>
                                <th>Role</th>
                                <th>Position</th>
                                <th>City</th>
                                <th>Company</th>
                                <th>Delete</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data as $key=>$each)
                                <tr>
                                    <td>
                                        <a class="" href="{{ route("admin.$table.show", $each->id) }}">
                                            {{ $key+1 }}
                                        </a>
                                    </td>
                                    <td>
                                        <img src="{{ $each->avatar }}" width="50" class="rounded-circle">
                                    </td>
                                    <td>
                                        {{ $each->name }} <br>
                                        <a href="mailto: {{ $each->email }} ">
                                            {{ $each->email }}
                                        </a> <br>
                                        <a href="tel: {{ $each->phone }} ">
                                            {{ $each->phone }}
                                        </a> <br>
                                        {{ $each->gender_name }} <br>
                                    </td>
                                    <td>{{ $each->role_name }}</td>
                                    <td>{{ $each->position }}</td>
                                    <td>{{ $each->city }}</td>
                                    <td>{{ optional($each->company)->name }}</td>
                                    <td>
                                        <form action="{{ route("admin.$table.destroy", $each->id) }}" method="post">
                                            @csrf
                                            @method('DELETE')
                                            <Button class="btn btn-danger">Delete</Button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <nav>
                        <ul class="pagination pagination-rounded mb-0">
                            {{ $data->links() }}
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')
    <script>
        $(document).ready(function () {
            $('.select-filter').change(function () {
                $("#form-filter").submit();
            });
        });
    </script>
@endpush