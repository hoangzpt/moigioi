<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title> Web môi giới </title>
    <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
    <link rel="icon" type="image/png" href="../assets/img/favicon.png">
    <!--     Fonts and icons     -->
    <link rel="stylesheet" type="text/css"
        href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">

    <!-- CSS Files -->
    <link href="{{ asset('CSS/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('CSS/material-kit.css') }}" rel="stylesheet">
</head>

<body class="ecommerce-page">
    <!-- NAVBAR -->
    @include('layout_page.navbar')
    <!-- END NAVBAR -->


    <!-- HEADER -->
    @include('layout_page.header')
    <!-- END HEADER -->

    <div class="main main-raised">
        <!-- section -->
        <div class="section">
            <div class="container">
                @yield('content')
            </div>
        </div><!-- section -->
    </div>

    <!-- section -->



    <!-- FOOTER -->
    @include('layout_page.footer')
    <!-- END FOOTER -->



    <script src="{{ asset('JS/jquery.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('JS/bootstrap.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('JS/material.min.js') }}"></script>


    <!--	Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/   -->
    <script src="{{ asset('JS/nouislider.min.js') }}" type="text/javascript"></script>

    <!--    Control Center for Material Kit: activating the ripples, parallax effects, scripts from the example pages etc    -->
    <script src="{{ asset('JS/material-kit.js') }}" type="text/javascript"></script>

    <!--	Plugin for Tags, full documentation here: http://xoxco.com/projects/code/tagsinput/   -->
	<script src="{{ asset('JS/bootstrap-tagsinput.js') }}"></script>

    @stack('js')

</body>

</html>
