<div class="page-header header-filter header-small" data-parallax="true"
    style="background-image: url(&quot;{{ asset('images/ecommerce-tips2.jpg') }}&quot;); transform: translate3d(0px, 0px, 0px);">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="brand">
                    <h1 class="title">Recruitment Page!</h1>
                </div>
            </div>
        </div>
    </div>
</div>
