<nav class="navbar navbar-default navbar-fixed-top navbar-color-on-scroll navbar-transparent" color-on-scroll="100"
    id="sectionsNav">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{ route('applicant.welcome') }}">TopDEV.COM</a>
        </div>

        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav navbar-right">
                {{-- <li>
                    <a href="../index.html">
                        <i class="material-icons">apps</i> Components
                    </a>
                </li> --}}
                @auth
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <img src="{{ user()->avatar }}" style="height: 50px;" alt="" class="rounded-circle img-thumbnail">
                            {{ user()->name }}
                            <b class="caret"></b>
                        </a>
                        <ul class="dropdown-menu dropdown-with-icons">
                            <li>
                                <a href="{{ route('logout') }}">
                                    <i class="material-icons">logout</i> Logout
                                </a>
                            </li>
                        </ul>
                    </li>
                @endauth
                @guest
                    <li>
                        <a href="{{ route('login') }}" class="dropdown-toggle">
                            <i class="material-icons">login</i> Login
                        </a>
                    </li>
                @endguest

                {{-- <li class="button-container">
                    <a href="http://www.creative-tim.com/buy/material-kit-pro?ref=presentation" target="_blank"
                        class="btn btn-rose btn-round">
                        <i class="material-icons">shopping_cart</i> Buy Now
                    </a>
                </li> --}}
            </ul>
        </div>
    </div>
</nav>
