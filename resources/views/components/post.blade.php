<div class="col-md-6 col-lg-4">
    <div class="rotating-card-container manual-flip" style="height: 328.844px; margin-bottom: 30px;">
        <div class="card card-rotate">
            <div class="front" style="min-height: 328.844px;">

                @if ($posts->is_pinned)
                    <svg style="height: 20px; position: absolute; right: 10px; top: 10px;" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512"><!--!Font Awesome Free 6.5.1 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2023 Fonticons, Inc.--><path d="M32 32C32 14.3 46.3 0 64 0H320c17.7 0 32 14.3 32 32s-14.3 32-32 32H290.5l11.4 148.2c36.7 19.9 65.7 53.2 79.5 94.7l1 3c3.3 9.8 1.6 20.5-4.4 28.8s-15.7 13.3-26 13.3H32c-10.3 0-19.9-4.9-26-13.3s-7.7-19.1-4.4-28.8l1-3c13.8-41.5 42.8-74.8 79.5-94.7L93.5 64H64C46.3 64 32 49.7 32 32zM160 384h64v96c0 17.7-14.3 32-32 32s-32-14.3-32-32V384z"/></svg>
                @endif

                <div class="card-content">
                    <h5 class="category-social text-success">
                        <a href="{{ route('applicant.show', $posts) }}">
                            <i class="fa fa-newspaper-o"></i>
                            {{ $posts->job_title }}
                        </a>
                    </h5>
                    <h4 class="card-title">
                        <a href="#pablo">
                            {{ $languages }}
                        </a>
                    </h4>
                    <p class="card-description">
                        {{ $posts->district }} - {{ $posts->city }}

                    </p>
                    <div class="footer text-center d-flex">
                        <div class="author" style="float: left">
                            <a href="#pablo">
                               <img src="{{ asset('storage/' . $company->logo) }}" alt="" class="avatar img-raised">
                               <span>{{ $company->name }}</span>
                                <div class="ripple-container">
                                    <div class="ripple ripple-on ripple-out" style="left: 1172px; top: 20743px; background-color: rgb(60, 72, 88); transform: scale(12.75);"></div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div style="float: right; margin-top: 30px">
                        Salary: {{ $posts->salary_name }}
                    </div>
                    @if ($posts->expired)
                        <div class="text-danger" style="position: absolute; right: 10px; top: 90%;">
                            Expired
                        </div>
                    @endif
                </div>
            </div>

            <div class="back" style="min-height: 328.844px">
                <div class="card-content">
                    <br>
                    <h5 class="card-title">
                        Location
                    </h5>
                    <p class="card-description">
                        {{ $posts->district }} - {{ $posts->city }}
                    </p>
                    <div class="footer text-center">
                        <a href="" class="btn btn-rose btn-round">
                            <i class="material-icons">subject</i> Read
                        </a>
                        <a href="" class="btn btn-just-icon btn-round btn-twitter">
                            <i class="fa fa-twitter"></i>
                        </a>
                        <a href="" class="btn btn-just-icon btn-round btn-dribbble">
                            <i class="fa fa-dribbble"></i>
                        </a>
                        <a href="" class="btn btn-just-icon btn-round btn-facebook">
                            <i class="fa fa-facebook"></i>
                        </a>
                    </div>
                    <br>
                    <button type="button" name="button" class="btn btn-simple btn-round btn-rotate">
                        <i class="material-icons">refresh</i> Back...
                    </button>

                </div>
            </div>

        </div>
    </div>
</div>
